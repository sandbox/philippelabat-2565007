<?php

/**
 * @file
 */

/**
 * Implements hook_drush_command()
 */
function provision_drush_command() {
  return array(
    'sdm' => array(
      'description' => 'Executes tasks relative to site deployment',
      'aliases' => array('provision'),
      'arguments' => array(
        'env' => "The environment type. available environment: dev, prod. Defaults to prod.",
      ),
    ),
  );
}

/**
 * List of all actions required on every code provision.
 */
function drush_provision_sdm($env = 'prod') {
  require_once drupal_get_path('module', 'sdm') . '/includes/projectsetup.inc';

  $is_quick = drush_get_option('quick');
  $is_dev = $env == 'dev' ? TRUE : FALSE;

  // Enabling theme.
  drush_print('Enabling theme...');
  ProjectSetup::enable_theme();

  // Enabling/disabling modules.
  drush_print('Enablig/disabling modules...');
  ProjectSetup::enable_modules($is_dev);

  // Settings parameters.
  drush_print('Setting parameters...');
  ProjectSetup::variables();

  // Reverting features.
  drush_print('Reverting features...');
  drupal_flush_all_caches();
  features_revert();

  // Database updates.
  drush_print('Database updates...');
  drush_invoke_process('@self', 'updatedb', array('-y'));

  // Flushing caches.
  drush_print('Flushing caches...');
  drupal_flush_all_caches();
}
