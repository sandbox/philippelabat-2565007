<?php

 /**
  * @file
  * Deployment class.
  */

/**
 * Class Containing util methods to help deploy the application.
 */
abstract class ProjectSetup {

  /**
   * Initialisation of needed variables.
   */
  public static function variables() {
    variable_set('site_name', 'This is my website title, hehe!');
    variable_set('site_slogan', "I can't even!!!");
  }

  /**
   * Modules to be enabled/disabled.
   */
  public static function enable_modules($dev = FALSE) {
    $enable = array(
      'project_features',
      'contact',
      'features',
      'strongarm',
      'admin_menu',
      'admin_menu_toolbar',
      'simpletest',
    );

    $disable = array(
      'toolbar',
      'color',
      'overlay',
      'comment',
    );

    if ($dev) {
      $enable = array_merge($enable, self::dev_modules());
    }
    else {
      $disable += self::dev_modules();
    }

    module_enable($enable);
    module_disable($disable, FALSE);
  }

  /**
   * Enable the themes.
   */
  public static function enable_theme() {
    theme_enable(array('bootstrap'));
    variable_set('theme_default', 'bootstrap');
  }

  /**
   * List of dev modules.
   */
  public static function dev_modules() {
    return array(
      'views_ui',
      'search_krumo',
      'maillog',
    );
  }
}
